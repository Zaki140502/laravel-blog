<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('home', [HomeController::class, 'home']);

Route::get('register', [AuthController::class, 'registerPage']);

Route::post('register', [AuthController::class, 'register']);

Route::get('master', function(){
    return view('adminlte.partials.master');
});

Route::get('items', function(){
    return view('adminlte.items');
});

Route::get('table', function(){
    return view('adminlte.tables.table');
});

Route::get('datatable', function(){
    return view('adminlte.tables.data-table');
});

